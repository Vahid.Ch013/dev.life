﻿namespace Features.Domain
{
    public class Employee
    {
        public string FirstName { get; init; }
        public string LastName { get; init; }

        public Employee(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}