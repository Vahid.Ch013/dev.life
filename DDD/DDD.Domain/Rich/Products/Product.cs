﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using DDD.Domain.Rich.Categories;
using DDD.Domain.Rich.Specs;


namespace DDD.Domain.Rich.Products
{
    public class Product:Entity<long>
    {
        private List<ProductSpec> _productSpecs;

        public string Name { get;  }
        public virtual Category Category { get; }
        public DateTimeOffset DateTime { get; }
        public virtual List<ProductSpec> ProductSpecs => _productSpecs;


        public Product(string name)
        {
            Name = name;
            DateTime=DateTimeOffset.Now;
            _productSpecs = new List<ProductSpec>();
        }
        
        public Result AddProductSpec(ProductSpec productSpec)
        {
            if(ProductSpecs.Any(x=>x.Spec.Id==productSpec.Spec.Id && x.ProductId==Id))
                return Result.Failure("spec already exists");

            _productSpecs.Add(productSpec);
            return Result.Success();

        }
    }
}