﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;
using DDD.Domain.Rich.Specs;

namespace DDD.Domain.Rich.Products
{
    public class ProductSpec:Entity<long>
    {

        public string Value { get; }
        public long ProductId { get; set; }
        public virtual Spec Spec { get; set; }
        public ProductSpec()
        {
            
        }
        public ProductSpec(string value,Spec spec)
        {
            Spec = spec;
            Value = value;
        }

        public static Result CanCreate(string value)
        {
            return Result.SuccessIf(string.IsNullOrWhiteSpace(value),
                value,"value is null or empty");
        }

        public static Result<ProductSpec> Create(string value,Spec spec)
        {
            return new ProductSpec(value, spec);
        }
    }
}