﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDD.Domain.Rich.Products.Configurations
{
    public class ProductSpecConfiguration:IEntityTypeConfiguration<ProductSpec>
    {
        public void Configure(EntityTypeBuilder<ProductSpec> builder)
        {
            builder.Property(x => x.Value).HasMaxLength(50);
            builder.HasAlternateKey("SpecId","ProductId").HasName("IX_MultipleColumns");

        }
    }
}