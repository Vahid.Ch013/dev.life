﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace DDD.Domain.Rich.ValueObjects
{
    public class Address:ValueObject
    {
        public string StreetAddress { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string ZipCode { get; private set; }
        protected Address()
        {
        }
        public Address(string streetAddress, string city, string state, string zipCode)
        {
            StreetAddress = streetAddress;
            City = city;
            State = state;
            ZipCode = zipCode;
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return StreetAddress;
            yield return City;
            yield return State;
            yield return ZipCode;
        }

        public static Result<Address> Create(string streetAddress,string city,string state,string zipCode)
        {
            return Result.FailureIf(
                (string.IsNullOrWhiteSpace(streetAddress) || string.IsNullOrWhiteSpace(city)
                                                          || string.IsNullOrWhiteSpace(city) ||
                                                          string.IsNullOrWhiteSpace(state) ||
                                                          string.IsNullOrWhiteSpace(zipCode))
                , new Address(streetAddress, city, state, zipCode),
                $"all fields for {nameof(Address)} must be inserted."
            );

        }
        
        public static Result<Address> CreateFromDb(string address)
        {
            var addressProp = address.Split("-");
            return new Address(addressProp[0], addressProp[1], addressProp[2], addressProp[3]);
        }
    }
}