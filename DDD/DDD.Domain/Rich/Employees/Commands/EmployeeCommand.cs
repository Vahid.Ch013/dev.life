﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Rich.ValueObjects;
using MediatR;

namespace DDD.Domain.Rich.Employees.Commands
{
    public class EmployeeCommand:IRequest<CreateEmployeePayload>
    {
        public string Firstname { get;  }
        public string LastName { get; }
        public string Street { get; }
        public string City { get;  }
        public string ZipCode { get; }
        public string State { get; }

        public EmployeeCommand(string firstname, string lastName, string street, string city, string zipCode, string state)
        {
            Firstname = firstname;
            LastName = lastName;
            Street = street;
            City = city;
            ZipCode = zipCode;
            State = state;
        }
        
        public class EmployeeCommandHandler:IRequestHandler<EmployeeCommand,CreateEmployeePayload>
        {
            private readonly EmployeeDbContext _dbContext;

            public EmployeeCommandHandler(EmployeeDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<CreateEmployeePayload> Handle(EmployeeCommand request, CancellationToken cancellationToken)
            {
                var address = Address.Create(request.Street, request.City, request.State, request.ZipCode);
                if (address.IsFailure)
                    return new CreateEmployeePayload(address.Error,null);
                var canCreateEmployee = Employee.CanCreate(request.Firstname, request.LastName, address.Value);
                if (canCreateEmployee.IsFailure)
                    return new CreateEmployeePayload(canCreateEmployee.Error,null);
                var employee = new Employee(request.Firstname, request.LastName, address.Value);
                _dbContext.Employees.Add(employee);
                await _dbContext.SaveChangesAsync(cancellationToken);
                return new CreateEmployeePayload(null,employee.Id);
            }
        }
    }
    public record CreateEmployeePayload(string? Error, long? Id);

}