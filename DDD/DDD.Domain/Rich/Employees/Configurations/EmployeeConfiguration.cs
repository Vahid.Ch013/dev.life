﻿using System.Linq;
using DDD.Domain.Rich.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;

namespace DDD.Domain.Rich.Employees.Configurations
{
    public class EmployeeConfiguration:IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(x => x.FirstName).HasMaxLength(50);
            builder.Property(x => x.LastName).HasMaxLength(100);
            
            // builder.Property(x => x.Address).HasConversion(
            //     write => write.City + "-" + write.StreetAddress + "-" + write.ZipCode,
            //     read =>
            //      Address.CreateFromDb(read).Value).IsRequired();

            builder.OwnsOne(x => x.Address, a =>
            {
                a.Property(x => x.City).HasConversion<string>();
                a.Property(x => x.StreetAddress).HasConversion<string>();
                a.Property(x => x.ZipCode).HasConversion<string>();
                a.Property(x => x.State).HasConversion<string>();
            });
            // builder.Property(x => x.Address)
            //     .HasConversion(write => JsonConvert.SerializeObject(write),
            //         read => JsonConvert.DeserializeObject<Address>(read));
        }
    }
}