﻿using System;
using CSharpFunctionalExtensions;
using DDD.Common;
using DDD.Domain.Events;
using DDD.Domain.Rich.ValueObjects;

namespace DDD.Domain.Rich.Employees            
{
    public class Employee:BaseEntity
    {
        public string FirstName { get; }
        public string LastName { get;  }
        public Address Address { get; }

        public Employee(string firstName, string lastName,Address address)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            AddEvent(new EmployeeCreated(firstName,lastName));
        }

        private Employee()
        {
            
        }

        public static Result CanCreate(string firstName,string lastName,Address address)
        {
            if (firstName is null)
            {
                throw new Exception("FirstName is null");
            }

            return Result.Success();
            //AddEvent(new EmployeeCreated());
            //return new Employee(firstName,lastName,address);
        }
        
    }
}