using CSharpFunctionalExtensions;

namespace DDD.Domain.Rich.Categories
{
    public class Category:Entity<long>
    {
        public string Name { get; set; }
    }
}