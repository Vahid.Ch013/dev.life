﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using DDD.Domain.Rich.Products;
using DDD.Domain.Rich.Specs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DDD.Domain.Repo
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAllProducts();
        Task<AddSpecPayLoad> AddSpecs(long specId,string value,long productId);
    }

    public class ProductRepository : IProductRepository
    {
        private readonly EmployeeDbContext _db;
        private readonly ILogger<IProductRepository> _logger;

        public ProductRepository(EmployeeDbContext db, ILogger<IProductRepository> logger)
        {
            _db = db;
            _logger = logger;
        }

        public async Task<List<Product>> GetAllProducts()
        {
            var products = await _db.Products.ToListAsync();
            _logger.LogInformation("GetAll Products called");
            return products;
        }
        
        public async Task<AddSpecPayLoad> AddSpecs(long specId,string value,long productId)
        {
           var spec =await _db.Spec.FirstOrDefaultAsync(x => x.Id == specId);
           if (spec == null) return new AddSpecPayLoad(null,"Spec not found");
            
           var product =await _db.Products.FirstOrDefaultAsync(x => x.Id == productId);
           if (product == null) return new AddSpecPayLoad(null,"Product not found");
           var productSpec = ProductSpec.Create(value, spec);
           
           var result = product.AddProductSpec(productSpec.Value);
           if (result.IsSuccess)
           {
               await _db.SaveChangesAsync();
               return new AddSpecPayLoad(productSpec.Value.Id, null);
           }

           return new AddSpecPayLoad(null, result.Error);
        }

      
    }
    public record AddSpecPayLoad(long? SpecId, string Error);

}