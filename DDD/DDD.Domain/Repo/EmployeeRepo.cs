﻿using System.Collections.Generic;
using System.Linq;
using DDD.Domain.Rich.Employees;

namespace DDD.Domain.Repo
{
    public interface IEmployeeRepo
    {
        List<Employee> GetAllEmployees();
    }

    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly EmployeeDbContext _employeeDbContext;

        public EmployeeRepo(EmployeeDbContext employeeDbContext)
        {
            _employeeDbContext = employeeDbContext;
        }

        public List<Employee> GetAllEmployees()
        {
            return _employeeDbContext.Employees.ToList();
        }
    }
}