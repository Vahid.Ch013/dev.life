﻿using System.Reflection;
using DDD.Domain.Rich.Employees;
using DDD.Domain.Rich.Products;
using DDD.Domain.Rich.Specs;
using Microsoft.EntityFrameworkCore;

namespace DDD.Domain
{
    public class EmployeeDbContext:DbContext
    {
        
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Spec> Spec { get; set; }
        public EmployeeDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}