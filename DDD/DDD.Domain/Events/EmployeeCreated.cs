﻿using System.Threading;
using System.Threading.Tasks;
using DDD.Common.Mediator;
using DDD.EmailParser;
using MediatR;

namespace DDD.Domain.Events
{
    public class EmployeeCreated:IDomainEvent
    {
        public EmployeeCreated(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get;}
        public string LastName { get;  }

        public class EmployeeCreatedHandler : INotificationHandler<EmployeeCreated>
        {
            private readonly ISendEmail _sendEmail;

            public EmployeeCreatedHandler(ISendEmail sendEmail)
            {
                _sendEmail = sendEmail;
            }

            public async Task Handle(EmployeeCreated notification, CancellationToken cancellationToken)
            {
                await _sendEmail.Send(notification.FirstName, notification.LastName);
            }
        }
    }
}