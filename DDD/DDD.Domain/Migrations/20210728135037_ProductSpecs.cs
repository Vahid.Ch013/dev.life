﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDD.Domain.Migrations
{
    public partial class ProductSpecs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spec_Products_ProductId",
                table: "Spec");

            migrationBuilder.DropIndex(
                name: "IX_Spec_ProductId",
                table: "Spec");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Spec");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "Spec");

            migrationBuilder.DropColumn(
                name: "DateTime",
                table: "Products");

            migrationBuilder.CreateTable(
                name: "ProductSpec",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    SpecId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSpec", x => x.Id);
                    table.UniqueConstraint("IX_MultipleColumns", x => new { x.SpecId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_ProductSpec_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductSpec_Spec_SpecId",
                        column: x => x.SpecId,
                        principalTable: "Spec",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductSpec_ProductId",
                table: "ProductSpec",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductSpec");

            migrationBuilder.AddColumn<long>(
                name: "ProductId",
                table: "Spec",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "Spec",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "DateTime",
                table: "Products",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_Spec_ProductId",
                table: "Spec",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Spec_Products_ProductId",
                table: "Spec",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
