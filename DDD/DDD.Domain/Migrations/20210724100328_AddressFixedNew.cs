﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDD.Domain.Migrations
{
    public partial class AddressFixedNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Employees",
                newName: "Address_ZipCode");

            migrationBuilder.AddColumn<string>(
                name: "Address_City",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_State",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_StreetAddress",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address_City",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "Address_State",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "Address_StreetAddress",
                table: "Employees");

            migrationBuilder.RenameColumn(
                name: "Address_ZipCode",
                table: "Employees",
                newName: "Address");
        }
    }
}
