using System.Reflection;
using DDD.Common.Mediator;
using DDD.Domain;
using DDD.Domain.Repo;
using DDD.EmailParser;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Serilog;

namespace DDD.EndpointApi
{
    public class Startup
    {
         
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EmployeeDbContext>((serviceProvider,opt) =>
            {
                opt.UseSqlServer(Configuration.GetConnectionString("EmployeeDbConnection"))
                    .AddInterceptors(serviceProvider.GetService<DomainEventDispatcher>())
                    .UseLazyLoadingProxies();
            });
            services.AddScoped<DomainEventDispatcher>();
            services.AddScoped<IEmployeeRepo, EmployeeRepo>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ISendEmail, SendEmail>();
            services.AddMediatR(Assembly.GetAssembly(typeof(EmployeeDbContext)));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "DDD.EndpointApi", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DDD.EndpointApi v1"));
            }

            app.UseHttpsRedirection();
            app.UseSerilogRequestLogging();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}