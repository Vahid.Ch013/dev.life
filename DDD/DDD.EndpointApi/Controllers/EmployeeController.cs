﻿using System.Threading.Tasks;
using DDD.Domain.Rich.Employees.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DDD.EndpointApi.Controllers
{
    [ApiController]
    public class EmployeeController:ControllerBase
    {
        private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("api/addEmployee")]
        public async Task<CreateEmployeePayload> CreateEmployee(string firstName,string lastName,string street,string zipCode,string city,string state)
        {
           var result=await _mediator.Send(new EmployeeCommand(firstName, lastName, street, city, zipCode, state));
           return result;
        }



    }
}