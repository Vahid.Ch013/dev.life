﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using DDD.Domain.Repo;
using DDD.Domain.Rich.Products;
using Microsoft.AspNetCore.Mvc;

namespace DDD.EndpointApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        [Route("api/allProducts")]
        public async Task<List<Product>> AllProducts()
        {
            return await _productRepository.GetAllProducts();
        }
        [HttpPost]
        [Route("api/addProductSpec")]
        public async Task<AddSpecPayLoad> AddProductSpec(long specId,string value,long productId)
        {
            return await _productRepository.AddSpecs(specId,value,productId);
        }
    }
}