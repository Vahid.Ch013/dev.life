﻿using System.Collections.Generic;
using System.Reflection;
using CSharpFunctionalExtensions;
using DDD.Common.Mediator;

namespace DDD.Common
{
    public class BaseEntity:Entity<long>
    {
        private readonly List<IDomainEvent> _events = new List<IDomainEvent>();
        public IEnumerable<IDomainEvent> Events => _events;

        public void AddEvent(IDomainEvent @event)
        {
            _events.Add(@event);
        }

        public void ClearEvents()
        {
            _events.Clear();
        }
    }


}