﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace DDD.Common.Mediator
{
    public class DomainEventDispatcher:SaveChangesInterceptor
    {
        private readonly IMediator _mediator;

        public DomainEventDispatcher(IMediator mediator)
        {
            _mediator = mediator;
        }
        public override InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
        {
            return SavingChangesAsync(eventData, result).GetAwaiter().GetResult();
        }
        public override async ValueTask<int> SavedChangesAsync(SaveChangesCompletedEventData eventData, int result, CancellationToken cancellationToken = new())
        {
            await DispatchDomainEventsAsync(eventData.Context.ChangeTracker);

            return result;
        }
        


        private async Task DispatchDomainEventsAsync(ChangeTracker contextChangeTracker)
        {
            var domainEntities = contextChangeTracker
                .Entries<BaseEntity>()
                .Select(x => x.Entity)
                .Where(x => x.Events.Any())
                .ToList();

            foreach (var entity in domainEntities)
            {
                var events = entity.Events.ToArray();
                entity.ClearEvents();
                foreach (var domainEvent in events)
                {
                    await _mediator.Publish(domainEvent).ConfigureAwait(false);
                }
            }
        }
    }
}