﻿using System.Threading.Tasks;

namespace DDD.EmailParser
{
    public interface ISendEmail
    {
        Task Send(string firstName, string lastName);
    }
}