using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Razor;
using FluentEmail.Smtp;

namespace DDD.EmailParser
{
    public class SendEmail : ISendEmail
    {
        public async Task Send(string firstName,string lastName)
        {
            var sender = new SmtpSender(() => new SmtpClient("localhost")
            {
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Port = 25
            });

            var template = new StringBuilder();
            template.AppendLine("Dear @Model.FirstName @Model.LastName");
            template.AppendLine("<p>Thanks for registering. we hope you enjoy it.</p>");
            template.AppendLine("- your team");
            Email.DefaultSender = sender;
            Email.DefaultRenderer = new RazorRenderer();

            var email = await Email
                .From("V.Cheshmi@gmail.com")
                .To("test@test.com", "David")
                .Subject("Registration completed!")
                .UsingTemplate(template.ToString(), new {FirstName = firstName, LastName = lastName})
                .SendAsync();
        }
    }
}