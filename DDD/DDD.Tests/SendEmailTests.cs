﻿using System.Threading.Tasks;
using DDD.EmailParser;
using Xunit;

namespace DDD.Tests
{
    public class SendEmailTests
    {
        [Theory]
        [InlineData("Vahid","Cheshmy")]
        public async Task Can_Send_Email(string firstName,string lastName)
        {
            var sendEmail = new SendEmail();
            await sendEmail.Send(firstName, lastName);
        }
    }
}